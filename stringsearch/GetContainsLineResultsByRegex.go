package stringsearch

import (
	"regexp"

	"gitlab.com/auk-go/core/defaultcapacity"
	"gitlab.com/auk-go/strhelper/strhelpercore"
)

func GetContainsLineResultsByRegex(
	contentsLines []string,
	regexp *regexp.Regexp,
) *strhelpercore.StringResultsMap {
	length := len(contentsLines)

	if length == 0 {
		return strhelpercore.EmptyStringResultsMap()
	}

	capacity := defaultcapacity.OfSearch(length)
	currentMap := strhelpercore.NewStringResultsMap(capacity)

	for index, currentLine := range contentsLines {
		if regexp.MatchString(currentLine) {
			result := &strhelpercore.StringResult{
				FoundIndex: index,
				Line:       currentLine,
				IsFound:    true,
			}

			currentMap.AddFoundOnly(result)
		}
	}

	return currentMap
}
