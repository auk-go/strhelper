package isstrsinternal

func Empty(lines []string) bool {
	return lines == nil || len(lines) == 0
}
