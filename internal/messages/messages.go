package messages

const (
	SearchNullPanicMessage              = "s or findingString cannot be nil."
	SliceOrFindingElementsAreNilOrEmpty = "Empty elements either in slice or finding elements."
)
