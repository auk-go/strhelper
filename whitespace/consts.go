package whitespace

const (
	maxUnit8    = 255
	doubleSpace = "  "
	tripleSpace = "   "
)
