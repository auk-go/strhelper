package whitespace

import "gitlab.com/auk-go/core/constants"

var (
	asciiSpaces = constants.AsciiSpace
	// Reference :
	// - https://en.wikipedia.org/wiki/Newline,
	// - https://en.wikipedia.org/wiki/Whitespace_character
	// - https://en.wikipedia.org/wiki/Regular_expression#Character_classes
	asciiNewLinesChars = constants.AsciiNewLinesChars
)
