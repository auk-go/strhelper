module gitlab.com/auk-go/strhelper

go 1.17.8

require (
	github.com/smartystreets/goconvey v1.8.1
	gitlab.com/auk-go/core v1.4.3
	gitlab.com/auk-go/enum v0.5.3
	gitlab.com/auk-go/errorwrapper v1.2.0
)

require (
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smarty/assertions v1.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
