package strhelpercore

type LineArgs struct {
	Content string
	Lines   []string
	Index   int
	Line    string
}
