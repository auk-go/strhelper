package strhelpercore

type GenericProcessor func(args *GenericProcessorArgs) interface{}
