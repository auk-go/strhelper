package strhelpercore

type StringWrapperRuneLoopArgs struct {
	Content *string
	Runes   *[]rune
	Index   int
	Rune    rune
}
