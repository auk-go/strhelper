package strhelpercore

type GenericProcessorArgs struct {
	Content              interface{}
	RawSplitContents     []interface{}
	Index                int
	SingleContentAtIndex interface{}
}
