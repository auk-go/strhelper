package strhelpercore

type ReplaceRequest struct {
	Text string
	*ReplaceIndividualRequest
}
