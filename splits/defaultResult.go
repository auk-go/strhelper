package splits

import "gitlab.com/auk-go/core/constants"

func defaultResult() []string {
	return []string{constants.EmptyString}
}
