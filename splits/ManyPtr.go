package splits

import (
	"gitlab.com/auk-go/core/constants"

	"gitlab.com/auk-go/strhelper/strhelpercore"
	"gitlab.com/auk-go/strhelper/stringindex"
	"gitlab.com/auk-go/strhelper/whitespace"
)

// ManyPtr Multiple split occur from the given array of splits.
//
// Basics of split("Hello World", " ") -> ["Hello", "World"] splitter will not be available in the result.
//
// limits :
//  - number of times split will be performed for all
//  - if -1 then all split will occur
//
// splitStartsAt:
//  - where split searching will start from.
func ManyPtr(
	str string,
	splitsBy []string,
	splitStartsAt,
	limits int,
	isCaseSensitive bool,
) *strhelpercore.SplitResultOverview {
	if str == "" || len(splitsBy) == 0 {
		return strhelpercore.NewEmptySplitResultOverview(str)
	}

	allIndexes := stringindex.OfAllMany(
		str,
		&splitsBy,
		splitStartsAt,
		limits,
		isCaseSensitive)

	if allIndexes == nil || !allIndexes.HasResult() {
		return strhelpercore.NewEmptySplitResultOverview(str)
	}

	indexesAsKeyMap := allIndexes.GetIndexesMapWhereIndexAsKey()
	possibleCapacity := len(indexesAsKeyMap) + constants.ArbitraryCapacity5
	results := make([]string, 0, possibleCapacity)
	nonResults := make([]string, 0, possibleCapacity)
	splitResults := make([]*strhelpercore.SplitResult, 0, possibleCapacity)
	nonEmptySplitResults := make([]*strhelpercore.SplitResult, 0, possibleCapacity)
	lastIndexOfSplit := 0
	isLimitSet := limits > -1
	var isEmptyWord bool

	for ; splitStartsAt <= allIndexes.LastIndexFound; splitStartsAt++ {
		if isLimitSet && limits <= 0 {
			break
		}

		searchStr, isIndexExist := indexesAsKeyMap[splitStartsAt]
		isIndexExist = isIndexExist && lastIndexOfSplit <= splitStartsAt
		if isIndexExist {
			word := str[lastIndexOfSplit:splitStartsAt]
			isEmptyWord = word == "" || whitespace.IsAsciiWhitespaces(word)

			splitResult := strhelpercore.SplitResult{
				SplitPrev: word,
				Separator: searchStr,
				Index:     splitStartsAt,
				IsEmpty:   isEmptyWord,
			}

			results = append(results, word)
			splitResults = append(splitResults, &splitResult)

			if !isEmptyWord {
				nonResults = append(nonResults, word)
				nonEmptySplitResults = append(nonEmptySplitResults, &splitResult)
			}

			lastIndexOfSplit = splitStartsAt + len(searchStr)
		}

		if isLimitSet && isIndexExist {
			limits--
		}
	}

	resultsOverview := strhelpercore.SplitResultOverview{
		Results:              results,
		NonEmptyResults:      nonResults,
		NonEmptySplitResults: nonEmptySplitResults,
		SplitResults:         splitResults,
		IsEmptyResult:        !allIndexes.HasResult(),
	}

	return &resultsOverview
}
