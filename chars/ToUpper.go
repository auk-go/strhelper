package chars

import (
	"gitlab.com/auk-go/core/constants"
)

func ToUpper(c uint8) uint8 {
	if c >= constants.LowerCaseA &&
		c <= constants.LowerCaseZ {
		return c + constants.UpperCaseA - constants.LowerCaseA
	}

	return c
}
