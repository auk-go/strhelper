package strsindex

import (
	"gitlab.com/auk-go/core/constants"

	"gitlab.com/auk-go/strhelper/internal/panichelper"
	"gitlab.com/auk-go/strhelper/strhelpercore"
	"gitlab.com/auk-go/strhelper/strs"
)

// Returns all indexes by searching all @findingMap items.
//
// @limits:
//  - How many indexes should we search for and then stop looking further.
//  - `-1` means find all
//
// Results:
//  - Invalid result can be nil if any lines == nil results nil.
//  - If no indexes found returns nil.
func OfAllMany(
	lines []string,
	searchTerms []string,
	startsAtIndex int,
	limits int,
	isCaseSensitive bool,
) *strhelpercore.IndexesResultSet {
	if len(lines) == 0 || len(searchTerms) == 0 {
		return nil
	}

	searchingItemsLength := len(searchTerms)

	if searchingItemsLength == 0 {
		return strhelpercore.NewEmptyIndexesResultSet()
	}

	length := len(lines)

	if startsAtIndex <= constants.InvalidNotFoundCase || startsAtIndex > length-1 {
		panichelper.StartAtIndexFailed(startsAtIndex, length)
	}

	// making a copy of pointer only, not the object. copy of reference address
	// reference : https://play.golang.org/p/r65MrCg86YH
	sendingLines := lines
	sendingSearchTerms := searchTerms

	if isCaseSensitive == false {
		// insensitive
		sendingLines = strs.ToLowerStrings(lines)
		sendingSearchTerms = strs.ToLowerStrings(searchTerms)
	}

	indexesMap := make(map[string][]int, searchingItemsLength)
	var hasFoundAny bool
	var totalLength, maxIndexFound int
	maxIndexFound = -1

	for _, searchTerm := range sendingSearchTerms {
		indexes := OfAll(
			sendingLines,
			searchTerm,
			startsAtIndex,
			limits,
			true)

		if len(indexes) == 0 {
			indexesMap[searchTerm] = nil

			continue
		}

		totalLength = len(indexes)
		indexesMap[searchTerm] = indexes
		hasFoundAny = true
		lastIndex := indexes[totalLength-1]

		if maxIndexFound < lastIndex {
			maxIndexFound = lastIndex
		}
	}

	return strhelpercore.NewIndexesResultSet(
		indexesMap,
		hasFoundAny,
		maxIndexFound)
}
