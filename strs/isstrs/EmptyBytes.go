package isstrs

func EmptyBytes(lines []byte) bool {
	return len(lines) == 0
}
