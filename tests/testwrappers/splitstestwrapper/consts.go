package splitstestwrapper

import (
	"gitlab.com/auk-go/core/coretests"
)

const (
	last        coretests.TestFuncName = "Last"
	lastByRunes coretests.TestFuncName = "LastByRunes"
	lastByRune  coretests.TestFuncName = "LastByRune"
)
