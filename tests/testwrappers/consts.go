package testwrappers

import "gitlab.com/auk-go/core/coretests"

const (
	isEndsWith   coretests.TestFuncName = "IsEndsWith"
	isStartsWith coretests.TestFuncName = "IsStartsWith"
)
