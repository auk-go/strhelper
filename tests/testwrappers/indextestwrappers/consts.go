package indextestwrappers

import "gitlab.com/auk-go/core/coretests"

const (
	ofAllPtr     coretests.TestFuncName = "OfAllPtr"
	ofLastAllPtr coretests.TestFuncName = "OfLastAllPtr"
)
