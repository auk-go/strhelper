package isstr

// Returns :
//  - true : if both are equal based on case sensitivity.
func EqualsSensitive(first, second string) bool {
	return first == second
}
